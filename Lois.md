# Lois

## Préambule

Les Lois protègent les joueurs des déviances du jeu. Ce document ne doit jamais être modifié.

## Lois

1. **Loi d'Éternité:** Quoi qu'il arrive, ces Lois sont toujours au-dessus de toute autre chose faisant partie du jeu. Elles ne peuvent être modifiées ou remplacées d'aucune manière et s'appliquent en tout temps à chaque joueur.
1. **Loi de Sécurité:** Un joueur a toujours le droit de choisir de quitter le jeu définitivement au lieu de réaliser une action.
1. **Loi de Justice:** Aucun élément du jeu ne peut être contraire aux lois applicables au lieu où se trouve physiquement le joueur, ni contraire aux Droits Humains. Les règles et les lois externes au jeu sont toujours au-dessus de celles dans le jeu.
1. **Loi de Sens Commun:** Si une règle du jeu est ambigüe ou prête à confusion, les joueurs tentent de l'interpréter au mieux pour le jeu, comme ils le feraient dans un jeu conventionnel. Si les joueurs n'arrivent pas à se mettre d'accord, un vote a lieu. Si le vote ne désigne aucune majorité, un tirage au sort a lieu. Dans tous les cas, les joueurs devraient clarifier ce point de règle dès que possible.
1. **Loi de Bonne Foi:** Aucun joueur n'a le droit de tricher ou d'utiliser une ruse non décrite dans le jeu afin de jouer.
1. **Loi de Confinement:** Le jeu ne peut jamais obliger un joueur à fournir quelque chose lui appartenant. En particulier, aucun joueur ne peut être obligé de mettre en jeu des objets ou des valeurs. Il peut en revanche prêter quelque chose au jeu. Dans ce dernier cas, la durée et les conditions du prêt doivent être définies par le prêteur au moment du prêt.
1. **Loi de Discrimination:** Aucun élément du jeu ne peut faire une différence entre les joueurs selon des critères externes au jeu. Cela s'applique entre autre, mais pas exclusivement, au sexe, à la couleur de peau, à la nationalité et à l'âge. Il est en revanche autorisé de définir des caractéristiques internes au jeu, et donc modifiables, et d'utiliser celles-ci à des fins discriminatoires.
1. **Loi de paradoxe:** Si un joueur a la possibilité de réaliser une action qui soit à la fois autorisée et interdite par le jeu, ce joueur a gagné et la partie s'arrête immédiatement.
1. **Loi de Conséquence:** Le non respect de ces Lois implique une expulsion immédiate et irrévocable du jeu.
