# Nomic Français

## Qu'est-ce que le Nomic ?

Le Nomic est un jeu créé par Peter Suber, dans le but de souligner le paradoxe d'auto-amendement, c'est-à-dire la possibilité qu'offre une loi de se modifier elle-même. Dans ce jeu, les joueurs vont modifier les règles afin de les faire évoluer vers… on ne sait pas encore quoi. Dans une partie de Nomic, rien n'est stable, rien n'est gravé dans le marbre. Tout peut changer.

## Cette partie-ci

Cette partie ne reprend pas les règles originelles de Peter Suber. Au lieu de cela, un jeu de règles simplifé a été créé. Il est bien sûr facile de le modifier afin d'en faire quelque chose d'autre.

Deux documents sont très importants :

- Les [Lois](Lois.md) sont des règles immuables et éternelles, au-dessus de toute autre partie du jeu. Leur but est de sécuriser la partie afin d'éviter les déviances.
- Les [Règles](Règles.md) sont la partie mouvante du jeu. Chacun se doit de les respecter, mais ce qui déplait peut être modifié.

## Comment participer ?

- Créez-vous un compte GitLab si ce n'est pas fait en utilisant le bouton "Register" en haut à droite.
- Assurez-vous d'être connecté à GitLab grâce au bouton "Sign In".
- Demandez de rejoindre le dépôt [play-nomic/nomic-francais](https://gitlab.com/play-nomic/nomic-francais) en cliquant [ici](https://gitlab.com/play-nomic/nomic-francais/-/project_members/request_access) et attendez que votre inscription soit validée.
- Pour modifier le jeu:
    1. Cliquez sur le bouton *Web IDE*.
    1. Effectuez les modification de votre choix.
    1. Dans le menu de gauche du Web IDE, cliquez sur la troisième icône (trois cercles connectés par des lignes, sans doute avec un nombre), puis cliquez sur *Commit & Push*. Lorsqu'on vous demande sur quelle branche pousser, créez-en une nouvelle (`main` est protégée).
    1. Quittez le Web IDE et revenez sur le projet.
    1. Créez un nouveau *Merge Request* de votre branche vers la branche `main`.
- Pour modifier une proposition, depuis le Web IDE, vous pouvez modifier la branche courante en bas à gauche. Choisissez la branche à modifier à la place de `main`. Si vous avez dlàjà effectué le *Merge Request*, il sera mis à jour automatiquement.

## Plus de questions

Ouvrez un ticket en cliquant sur *Issues* dans le menu de gauche du projet principal et placez-y votre question.