# Règles

1. **Point de règle**
    1. Un point de règle est défini par les éléments suivants, qui doivent être présents dans l'ordre indiqué:
        1. Un nombre, suivi par un point, puis par un espace. Dans la source, il s'agit toujours de `1. `, mais ceci est automatiquement changé visuellement dans la version affichable.
        1. Un texte:
            1. S'il s'agit d'un point de règle n'étant pas le sous-point d'une autre règle, un titre en gras.
            1. Sinon, un texte décrivant en détail le point de règle.
        1. Un ou plusieurs sous-points qui suivent le même schéma qu'un point de règle, mais précédé d'une tabulation de plus que le point de règle parent. Si le point de règle est lui-même sous-point d'une autre règle, ceci est facultatif.
    1. Un nouveau point de règle est ajouté après le dernier présent. Un nouveau sous-point de règle est ajouté après le dernier sous-point de la règle concernée.
    1. Si un point de règle est supprimé, les points de règles subséquents voient leur numéro changer automatiquement. Une règle donnée peut donc avoir des numéros différents au cours de la partie.
    1. Tous les points de règle doivent se trouver dans le fichier `Règles.md`.
1. **Joueur**
    1. Est considéré comme joueur toute personne physique inscrite sur GitLab et étant dans la liste des membres du projet.
    1. Une même personne possédant plusieurs compte sur GitLab ne peut rejoindre le projet qu'avec un seul de ses comptes.
    1. L'ordre de jeu est l'ordre alphanumérique des noms des joueurs. Le premier joueur est tiré au sort.
    1. Tout joueur qui change son nom sur GitLab perd son prochain tour de jeu.
    1. Si un nouveau joueur entre en jeu, il ne peut participer qu'au début du tour suivant.
    1. S'il y a moins de trois joueurs, le jeu est mis en pause, entre le tour de deux joueurs, jusqu'à ce que le nombre de joueur augmente à trois ou plus.
1. **Tour de jeu**
    1. À son tour de jeu, un joueur peut faire une proposition ou passer son tour.
    1. Une proposition est l'une des modifications de règles suivantes:
        1. Création d'une nouvelle règle ou d'un nouveau sous-point de règle.
        1. Modification d'une règle existante ou d'un sous-point de règle existant.
        1. Suppression d'une règle existante ou d'un sous-point de règle existant.
        1. Déplacement d'une règle existante ou d'un sous-point de règle existant.
        1. Une combinaison de deux ou plus des points ci-dessus.
    1. Aucune proposition ne peut être faite si elle implique une contradiction entre deux règles.
        1. Les règles transitoires font exception à cette règle.
    1. Une proposition est soumise sous la forme d'un *Merge Request*.
    1. Dès qu'un *Merge Request* est ouvert, tous les joueurs peuvent donner leur approbation en commentant "OUI", "NON" ou "BLANC" dans les commentaires du *Merge Request*. Le message ne peut contenir que ce mot, sans quoi il n'est pas considéré comme un vote valide.
    1. Si une modification a été faite après l'approbation d'un ou plusieurs joueurs, seuls les commentaires de vote qui suivent la dernière modification sont valides.
    1. Une semaine après l'ouverture du *Merge Request*, si le nombre de "OUI" est strictement supérieur au nombre de "NON", le *Merge Request* est accepté et les règles du jeu sont modifiées. Sinon, le *Merge Request* est refusé.
    1. Si plus de 50% des joueurs ont voté "OUI" avant la fin de la période de vote, le *Merge Request* est accepté sans attendre. De manière similaire, si plus de 50% des joueurs ont voté "NON" avant la fin de la période de vote, le *Merge Request* est refusé sans attendre.
    1. Le tour de jeu d'un joueur prend fin lorsque:
        1. Son *Merge Request* a été accepté ou refusé, ou
        1. Il a clairement signifier son intention de passer, ou
        1. Il n'a pas participé au jeu depuis une semaine.
1. **Arrêté**
    1. Un arrêté est une modification qui touche uniquement les éléments annexes au jeu et donc ne changent pas les règles. Il s'agit des fichiers ou parties de fichiers suivants:
        1. `README.md` (Aide de jeu)
        1. `.gitlab-ci.yml` (Script d'automatisation)
        1. Les règles transitoires obsolètes dans `Règles.md` (Règles inutiles)
    1. Un arrêté peut être fait par n'importe quel joueur, indépendamment de son tour de jeu. Il doit être soumis comme *Merge Request*.
    1. Un arrêté n'a pas besoin de passer par la phase de vote et peut être validé dès sa proposition.
1. **Règle transitoire**
    1. Les règles transitoires sont des règles temporaires permettant de faciliter le passage vers une version non rétrocompatible du jeu.
    1. Une règle transitoire doit posséder un sous-point indiquant à quel moment la règle devient obsolète.
        1. Si le sous-point d'une règle transitoire indique que celle-ci est obsolète, la règle transitoire ne doit plus être respectée, même si son texte en encore dans les règles.
    1. Les règles transitoires peuvent être en contradiction avec d'autres règles.
        1. Si une règle transitoire est en contradiction avec une règle non-transitoire, c'est la règle transitoire qui prévaut.
        1. Si deux règles transitoires sont en contradiction, c'est celle ayant le numéro le plus grand qui prévaut.
    1. Les règles transitoires doivent toutes être écrites dans les sous-points de la présente règle. Voici la liste exhaustive des règles transitoires:
        1. Cette règle s'appelle "règle transitoire générique" et n'a aucune utilité.
            1. Cette règle devient obsolète lorsqu'une autre règle transitoire entre en jeu.
    1. Si, lors de l'abrogation d'une ou plusieurs règles transitoires, par proposion, par arrêté ou par tout autre moyen, la liste des règles transitoires est vide, alors la règle transitoire générique y est ajoutée automatiquement.
    1. La règle transitoire générique a la teneur suivante: «Cette règle s'appelle "règle transitoire générique" et n'a aucune utilité.». Elle devient obsolète à l'ajout d'une nouvelle règle transitoire.
1. **But du jeu et fin de partie**
    1. Le premier joueur qui parvient à libérer la princesse gagne la partie.
    1. Lorsqu'un joueur gagne la partie, celle-ci est terminée.
